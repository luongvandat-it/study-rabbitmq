const amqplib = require('amqplib');
const amqp_url = 'your url';

const sendMessage = async () => {
    try {
        const connection = await amqplib.connect(amqp_url);
        const channel = await connection.createChannel();
        const exchangeName = 'datlv-exchange';

        await channel.assertExchange(exchangeName, 'topic', {
            durable: true,
        });

        const argvs = process.argv.slice(2);
        const topic = argvs[0];
        const message = argvs[1];
        console.log('topic:: ', topic, 'message:: ', message);

        await channel.publish(exchangeName, topic, Buffer.from(message));

        setTimeout(() => {
            connection.close();
            process.exit(0);
        }, 2000);
    } catch (error) {
        console.log("🚀 ~ sendMessage ~ error:", error);
    }
};

sendMessage();