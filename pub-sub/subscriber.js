const amqplib = require('amqplib');
const amqp_url = 'your url';

const sendMessage = async () => {
    try {
        const connection = await amqplib.connect(amqp_url);
        const channel = await connection.createChannel();
        const exchangeName = 'datlv-exchange';

        await channel.assertExchange(exchangeName, 'topic', {
            durable: true,
        });

        const { queue } = await channel.assertQueue('', {
            durable: true,
        });

        const argvs = process.argv.slice(2);
        
        if (!argvs.length) {
            process.exit(0);
        }

        await argvs.forEach( async key => {
            await channel.bindQueue(queue, exchangeName, key);
        })

        await channel.consume(queue, (message) => {
            console.log("🚀 ~ awaitchannel.consume ~ message:", message.content.toString())
        })
    } catch (error) {
        console.log("🚀 ~ sendMessage ~ error:", error);
    }
};

sendMessage();