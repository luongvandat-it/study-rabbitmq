const amqplib = require('amqplib');
const amqp_url = 'your url';

const sendMessage = async (message) => {
    const connection = await amqplib.connect(amqp_url);
    const channel = await connection.createChannel();
    const queueName = 'datlv';
    
    await channel.assertQueue(queueName, {
        durable: true,
    });
    
    await channel.sendToQueue(queueName, Buffer.from(message), {
        persistent: true,
        expiration: 10000,
    });
};

sendMessage('Welcome... 001');