const amqplib = require('amqplib');
const amqp_url = 'your url';

const receiveMessage = async () => {
    const connection = await amqplib.connect(amqp_url);
    const channel = await connection.createChannel();
    const queueName = 'datlv';
    
    await channel.assertQueue(queueName, {
        durable: true,
    });
    
    await channel.consume(queueName, (message) => {
        console.log("🚀 ~ await channel.consume ~ message:", message.content.toString())
    }, {
        noAck: false,
    })
};

receiveMessage();